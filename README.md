# ✨Pj Metz✨

## Educator turned DevRel
Pj Metz is an DevRel who previously worked at GitLab and a former high school English teacher. After 11 years in the classroom, he started learning to code in May of 2020 and began to live stream his sessions learning C# and Node.js shortly after. His most recent role was all about bringing DevSecOps into the classroom by making GitLab available to educators around the world. He has hosted a few workshops for students to get experience with GitLab and concepts they might not encounter in the classroom. He has presented on a variety of topics at several events like Start.Dev.Change, Codeland 2021, DevOpsDays Chicago, and DevRelCon. 

Pj loves 80’s music, including modern music that sounds like it was made in the 80’s, 90’s internet culture, and poetry. He led poetry reading events for GitLab team members and participated often in the Latinx, Pride, and Black @ Gitlab Team Member Resource Groups. Currently, you can find him on Twitch a few times each month learning about new languages or making Dicsord bots. 


